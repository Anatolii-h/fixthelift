﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixTheElevator.Models
{
    public class SimpleOrder
    {
        public string Description { get; set; }
        public string District { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public int EnteranceNumber { get; set; }

        public SimpleOrder() { }
    }
}