﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FixTheElevator.Model
{
    public class Order
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public DateTime dateTime { get; set; }
        public Guid? addressId { get; set; }
        public Address address { get; set; }
        public string Description { get; set; }
        public OrderStatus Status { get; set; }

        public Order()
        { }

        public Order(Address addr, string description, DateTime date)
        {
            dateTime = date;
            address = addr;
            Description = description;

            Status = OrderStatus.Not_Completed;
        }

        public override string ToString()
        {
            return "ORDER" +
                "\r\nID: " + Id +
                "\r\nPOST TIME: " + dateTime.ToString() +
                "\r\nSTATUS: " + Status.ToString() +
                "\r\nADDRESS:\r\n" + address.ToString() +
                "\r\nDESCRIPTION:\r\n" + Description;
        }
    }
}
