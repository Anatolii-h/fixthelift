﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FixTheElevator.Model
{
    public class Address
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string District { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public int? EnteranceNumber { get; set; }
        public int? Appartment { get; set; }

        public Address()
        { }

        public Address(string district, string street, string houseNumber, int? enteranceNumber)
        {
            District = district;
            Street = street;
            HouseNumber = houseNumber;
            EnteranceNumber = enteranceNumber;

            Appartment = null;
        }

        public Address(string district, string street, string houseNumber, int? enteranceNumber, int? appartment)
        {
            District = district;
            Street = street;
            HouseNumber = houseNumber;
            EnteranceNumber = enteranceNumber;

            Appartment = appartment;
        }

        public override string ToString()
        {
            string aptm = "";

            if (Appartment != null)
                aptm = Appartment.HasValue ? "\r\nAPPARTMENT: " + Appartment.Value.ToString() : "";

            string enterNum = "";

            if (EnteranceNumber != null)
                enterNum = EnteranceNumber.HasValue ? "\r\nENTERANCE NUMBER: " + EnteranceNumber.Value.ToString() : "";

            return "DISTRICT: " + District +
                "\r\nSTREET: " + Street +
                "\r\nHOUSE NUMBER: " + HouseNumber +
                enterNum + aptm;
        }
    }
}
