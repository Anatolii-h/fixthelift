﻿namespace FixTheElevator.Model
{
    class DispetcherAccount : Account
    {
        public int Salary { get; set; }

        public DispetcherAccount()
        { }

        public DispetcherAccount(string firstName, string lastName, Contacts contact, string password, int salary) : base(firstName, lastName, contact, password)
        {
            Salary = salary;
        }
        
        public override string ToString()
        {
            return "ACCOUNT TYPE: Dispetcher" +
                "\r\nSALARY: " + Salary +
                "\r\n" + "ID: " + Id +
                "\r\nFIRST NAME: " + FirstName +
                "\r\nLAST NAME: " + LastName +
                "\r\nPASSWORD: " + Password +
                "\r\nCONTACTS:\r\n" + Contact.ToString();
        }
    }
}
