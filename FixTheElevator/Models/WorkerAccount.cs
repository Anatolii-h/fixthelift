﻿using System;
using System.Collections.Generic;
using FixTheElevator.Exceptions;

namespace FixTheElevator.Model
{
    class WorkerAccount : Account, IComparable
    {
        public int Salary { get; set; }
        public WorkerStatus Status { get; set; }
        public Guid? CurrentOrderId { get; set; }
        public Order CurrentOrder { get; set; }
        public List<Order> CompletedOrders { get; set; }

        public WorkerAccount()
        { }

        public WorkerAccount(string firstName, string lastName, Contacts contact, string password, int salary) : base(firstName, lastName, contact, password)
        {
            CompletedOrders = new List<Order>();
            CurrentOrder = null;
            Salary = salary;
            Status = WorkerStatus.Free;
        }
        
        int IComparable.CompareTo(object obj)
        {
            WorkerAccount another = obj as WorkerAccount;

            return Id.CompareTo(another.Id);
        }

        public override string ToString()
        {
            string compOrd = "";

            if (CompletedOrders.Count > 0)
            {
                compOrd = "\r\nCOMPLETED ORDERS: " + CompletedOrders.Count + ":";

                foreach (Order ord in CompletedOrders)
                {
                    compOrd += "\r\n" + ord.Id;
                }
            }

            string currentOrdStr = CurrentOrder == null ? "" : "\r\nCURRENT ORDER:\r\n" + CurrentOrder.ToString();

            return "ACCOUNT TYPE: Worker" +
                "\r\nSALARY: " + Salary +
                "\r\n" + "ID: " + Id +
                "\r\nFIRST NAME: " + FirstName +
                "\r\nLAST NAME: " + LastName +
                "\r\nPASSWORD: " + Password +
                "\r\nCONTACTS:\r\n" + Contact.ToString() + compOrd + currentOrdStr;
        }
    }
}
