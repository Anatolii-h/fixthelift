﻿namespace FixTheElevator.Model
{
    class AdministratorAccount : Account
    {
        public int Salary { get; set; }

        public AdministratorAccount()
        { }

        public AdministratorAccount(string firstName, string lastName, Contacts contact, string password, int salary) : base(firstName, lastName, contact, password)
        {
            Salary = salary;
        }

        public override string ToString()
        {
            return "ACCOUNT TYPE: Administrator" +
                "\r\nSALARY: " + Salary +
                "\r\n" + "ID: " + Id +
                "\r\nFIRST NAME: " + FirstName +
                "\r\nLAST NAME: " + LastName +
                "\r\nPASSWORD: " + Password +
                "\r\nCONTACTS:\r\n" + Contact.ToString();
        }
    }
}
