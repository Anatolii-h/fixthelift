﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FixTheElevator.Model
{
    class Contacts
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string Phone { get; set; }
        public string EMail { get; set; }
        public Guid? addressId { get; set; }
        public Address address { get; set; }

        public Contacts()
        { }

        public Contacts(string phone, string email, Address addr)
        {
            Phone = phone;
            EMail = email;
            address = addr;
        }

        public override string ToString()
        {
            return "PHONE: " + Phone +
                "\r\nEMAIL: " + EMail +
                "\r\nADDRESS:\r\n" + address.ToString();
        }
    }
}
