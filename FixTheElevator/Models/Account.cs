﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FixTheElevator.Model
{
    class Account
    {
        [Key, DatabaseGenerated (DatabaseGeneratedOption.Identity)]
        public Guid Id { get; private set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Guid? ContactId { get; set; }
        public Contacts Contact { get; set; }
        public string Password { get; set; }
        public string AccountInfo;

        public Account()
        { }

        public Account(string firstName, string lastName, Contacts contact, string password)
        {
            FirstName = firstName;
            LastName = lastName;
            Contact = contact;
            Password = password;

            AccountInfo = "ID: " + Id +
                "\nFIRST NAME: " + firstName +
                "\nLAST NAME: " + lastName +
                "\nPASSWORD: " + password +
                "\nCONTACTS:\n" + Contact.ToString();
        }
    }
}
