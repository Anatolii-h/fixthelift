﻿using FixTheElevator.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixTheElevator.Dto
{
    class AccountDto
    {
        public Guid Id { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public Contacts Contact { get; private set; }

        public AccountDto (Guid id, string fname, string lname, Contacts contacts)
        {
            Id = id;
            FirstName = fname;
            LastName = lname;
            Contact = contacts;
        }

        protected IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<object> { Id, FirstName, LastName, Contact };
        }


        public override string ToString()
        {
            return string.Format("AccountId = {0}\nFirst name = {1}\nLast name = {2}\nContacts : {3}\n", Id, FirstName, LastName, Contact.ToString());
        }
    }
}
