﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixTheElevator.Dto
{
    class AdressDto
    {
        public Guid Id { get; private set; }
        public string District { get; private set; }
        public string Street { get; private set; }
        public string HouseNumber { get; private set; }
        public int? Enterance { get; private set; }

        public AdressDto(Guid id, string district, string street, string houseNumber, int? enterance)
        {
            Id = id;
            District = district;
            Street = street;
            HouseNumber = houseNumber;
            Enterance = enterance;
        }

        protected IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<object> { Id, District, Street, HouseNumber, Enterance };
        }


        public override string ToString()
        {
            return string.Format("Id = {0}\nDistrict = {1}\nStreet = {2}\nHouse number = {3}\nEnterance = {4}\n"
                , Id, District, Street, HouseNumber, Enterance.HasValue == true? Enterance.Value : -1);
        }
    }
}
