﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FixTheElevator.Model;

namespace FixTheElevator.Dto
{
    class OrderDto
    {
        public Guid Id { get; private set; }
        public DateTime dateTime { get; private set; }
        public Address address { get; private set; }
        public string Description { get; private set; }
        public OrderStatus Status { get; private set; }

        public OrderDto(Guid id, DateTime dateTime, Address address, string description, OrderStatus status)
        {
            Id = id;
            this.dateTime = dateTime;
            this.address = address;
            Description = description;
            Status = status;
        }

        protected IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<object>() { Id, dateTime, address, Description, Status };
        }


        public override string ToString()
        {
            return string.Format("\tOrderId = {0}\n\tTime = {1}\n\tAddress = {2}\n\tDescription = {3}\n\tStatus = {4}\n",
                                  Id, dateTime, address, Description, Status);
        }
    }
}
