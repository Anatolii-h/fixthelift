﻿using FixTheElevator.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixTheElevator.Dto
{
    class AdministratorAccountDto : AccountDto
    {
        public AdministratorAccountDto(Guid id, string fname, string lname, Contacts contacts) : base(id, fname, lname, contacts)
        { }
    }
}
