﻿using System;
using System.Collections.Generic;
using FixTheElevator.Model;

namespace FixTheElevator.Dto
{
    class ContactsDto
    {
        public Guid Id { get; private set; }
        public string Phone { get; private set; }
        public string EMail { get; private set; }
        public Address address { get; private set; }

        public ContactsDto(Guid id, string phone, string email, Address address)
        {
            Id = id;
            Phone = phone;
            EMail = email;
            this.address = address;
        }

        protected IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<object>() { Id, Phone, EMail, address };
        }


        public override string ToString()
        {
            return string.Format("\tId = {0}\n\tPhone = {1}\n\tEMail = {2}\n\tAddress : {3}\n",
                                  Id, Phone, EMail, address.ToString());
        }
    }
}
