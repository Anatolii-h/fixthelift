﻿using System.Reflection;

namespace FixTheElevator.Service.Impl
{
    static class ServiceAssemblyInformer
    {
        public static AssemblyName assemblyName { get; set; }

        static ServiceAssemblyInformer()
        {
            assemblyName = Assembly.GetExecutingAssembly().GetName();
        }
    }
}
