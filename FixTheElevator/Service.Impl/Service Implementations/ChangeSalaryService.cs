﻿using FixTheElevator.Model;
using FixTheElevator.Repository;
using System;

namespace FixTheElevator.Service.Impl
{
    class ChangeSalaryService : IChangeSalaryService
    {
        private IAdministratorAccountRepository administratorAccountRepository;
        private IDispetcherAccountRepository dispetcherAccountRepository;
        private IWorkerAccountRepository workerAccountRepository;

        public ChangeSalaryService(IAdministratorAccountRepository administratorAccountRepository
            , IDispetcherAccountRepository dispetcherAccountRepository
            , IWorkerAccountRepository workerAccountRepository)
        {
            this.administratorAccountRepository = administratorAccountRepository;
            this.dispetcherAccountRepository = dispetcherAccountRepository;
            this.workerAccountRepository = workerAccountRepository;
        }

        void IChangeSalaryService.ChangeAdministratorsSalary(int newSalary)
        {
            administratorAccountRepository.StartTransaction();

            AdministratorAccount a = administratorAccountRepository.GetAdministrator();
            a.Salary = newSalary;

            administratorAccountRepository.Commit();
        }

        void IChangeSalaryService.ChangeDispetchersSalary(int newSalary)
        {
            dispetcherAccountRepository.StartTransaction();

            DispetcherAccount a = dispetcherAccountRepository.GetDispetcher();
            a.Salary = newSalary;

            dispetcherAccountRepository.Commit();
        }

        void IChangeSalaryService.ChangeWorkersSalary(Guid Id, int newSalary)
        {
            workerAccountRepository.StartTransaction();

            WorkerAccount a = workerAccountRepository.FindWorkerById(Id);
            a.Salary = newSalary;

            workerAccountRepository.Commit();
        }
    }
}
