﻿using FixTheElevator.Model;
using FixTheElevator.Repository;
using System;

namespace FixTheElevator.Service.Impl
{
    class ManageWorkerService : IManageWorkerService
    {
        private IWorkerAccountRepository workerAccountRepository;
        private IOrderRepository orderRepository;

        public ManageWorkerService(IWorkerAccountRepository workerAccountRepository, IOrderRepository orderRepository)
        {
            this.workerAccountRepository = workerAccountRepository;
            this.orderRepository = orderRepository;
        }

        void IManageWorkerService.AssignOrderToWorker(Guid workerId, Guid orderId)
        {
            var workerAccount = workerAccountRepository.FindWorkerById(workerId);
            var aorder = orderRepository.GetOrderById(orderId);

            if (workerAccount.Status == WorkerStatus.Free)
            {
                if (aorder.Status == OrderStatus.Completed)
                {
                    throw new Exceptions.WorkingProcessException("Order is completed");
                }

                workerAccountRepository.StartTransaction();

                workerAccount.CurrentOrder = aorder;
                workerAccount.CurrentOrderId = aorder.Id;
                workerAccount.Status = WorkerStatus.Busy;

                workerAccountRepository.Commit();

                orderRepository.StartTransaction();

                aorder.Status = OrderStatus.Not_Completed;

                orderRepository.Commit();
            }
            else
            {
                throw new Exceptions.ImproperWorkerStatusException(WorkerStatus.Busy, "Worker is busy");
            }
        }

        void IManageWorkerService.CompleteOrder(Guid workerId)
        {
            var workerAccount = workerAccountRepository.FindWorkerById(workerId);

            if (workerAccount.Status == WorkerStatus.Busy)
            {
                var order = orderRepository.GetOrderById(workerAccount.CurrentOrder.Id);

                orderRepository.StartTransaction();

                order.Status = OrderStatus.Completed;

                orderRepository.Commit();

                workerAccountRepository.StartTransaction();

                workerAccount.CurrentOrder = null;
                workerAccount.CurrentOrderId = null;
                workerAccount.Status = WorkerStatus.Free;

                workerAccount.CompletedOrders.Add(order);

                workerAccountRepository.Commit();
            }
            else
            {
                throw new Exceptions.ImproperWorkerStatusException(WorkerStatus.Free, "Worker is free");
            }
        }
    }
}
