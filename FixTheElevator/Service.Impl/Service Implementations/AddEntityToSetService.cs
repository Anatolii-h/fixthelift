﻿using FixTheElevator.Model;
using FixTheElevator.Repository;

namespace FixTheElevator.Service.Impl
{
    class AddEntityToSetService : IAddEntityToSetService
    {
        private IAdministratorAccountRepository administratorAccountRepository;
        private IDispetcherAccountRepository dispetcherAccountRepository;
        private IWorkerAccountRepository workerAccountRepository;
        private IOrderRepository orderRepository;

        public AddEntityToSetService(IAdministratorAccountRepository administratorAccountRepository
            ,IDispetcherAccountRepository dispetcherAccountRepository
            , IWorkerAccountRepository workerAccountRepository
            , IOrderRepository orderRepository)
        {
            this.administratorAccountRepository = administratorAccountRepository;
            this.dispetcherAccountRepository = dispetcherAccountRepository;
            this.workerAccountRepository = workerAccountRepository;
            this.orderRepository = orderRepository;
        }

        void IAddEntityToSetService.AddAdministrator(AdministratorAccount administrator)
        {
            administratorAccountRepository.StartTransaction();

            administratorAccountRepository.Add(administrator);

            administratorAccountRepository.Commit();
        }

        void IAddEntityToSetService.AddDispetcher(DispetcherAccount dispetcher)
        {
            dispetcherAccountRepository.StartTransaction();

            dispetcherAccountRepository.Add(dispetcher);

            dispetcherAccountRepository.Commit();
        }

        void IAddEntityToSetService.AddOrder(Order order)
        {
            orderRepository.StartTransaction();

            orderRepository.Add(order);

            orderRepository.Commit();
        }

        void IAddEntityToSetService.AddWorker(WorkerAccount worker)
        {
            workerAccountRepository.StartTransaction();

            workerAccountRepository.Add(worker);

            workerAccountRepository.Commit();
        }
    }
}
