﻿using System;
using FixTheElevator.Dto;
using FixTheElevator.Repository;

namespace FixTheElevator.Service.Impl
{
    class FindWorkerService : IFindWorkerService
    {
        private IWorkerAccountRepository workerAccountRepository;

        public FindWorkerService(IWorkerAccountRepository workerAccountRepository)
        {
            this.workerAccountRepository = workerAccountRepository;
        }

        WorkerAccountDto IFindWorkerService.FindFreeWorkerByDistrict(string district)
        {
            var workerAccount = workerAccountRepository.FindFreeWorkerByDistrict(district);

            return new WorkerAccountDto(workerAccount.Id, workerAccount.FirstName, workerAccount.LastName, workerAccount.Contact);
        }

        WorkerAccountDto IFindWorkerService.FindWorkerById(Guid id)
        {
            var workerAccount = workerAccountRepository.FindWorkerById(id);

            return new WorkerAccountDto(workerAccount.Id, workerAccount.FirstName, workerAccount.LastName, workerAccount.Contact);
        }

        WorkerAccountDto IFindWorkerService.FindWorkerByName(string firstName, string lastName)
        {
            var workerAccount = workerAccountRepository.FindWorkerByName(firstName, lastName);

            return new WorkerAccountDto(workerAccount.Id, workerAccount.FirstName, workerAccount.LastName, workerAccount.Contact);
        }
    }
}
