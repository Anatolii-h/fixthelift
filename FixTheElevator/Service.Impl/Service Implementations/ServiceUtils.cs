﻿using FixTheElevator.Repository;
using System;
using FixTheElevator.Model;

namespace FixTheElevator.Service.Impl
{
    sealed class ServiceUtils
    {
        private ServiceUtils() { }

        public static Order ResolveOrder(IOrderRepository repository, Guid Id)
        {
            Order entity = repository.GetOrderById(Id);

            if (entity != null)
                return entity;

            throw new ArgumentException("Unresolved entity");
        }

        public static Contacts ResolveOrder(IContactsRepository repository, Guid Id)
        {
            Contacts entity = repository.GetContactsById(Id);
            if (entity != null)
                return entity;

            throw new ArgumentException("Unresolved entity");
        }

        public static Address ResolveAddress(IAddressRepository repository, Guid Id)
        {
            Address entity = repository.GetAddressById(Id);
            if (entity != null)
                return entity;

            throw new ArgumentException("Unresolved entity");
        }

        public static WorkerAccount ResolveWorker(IWorkerAccountRepository repository, Guid Id)
        {
            WorkerAccount entity = repository.FindWorkerById(Id);
            if (entity != null)
                return entity;

            throw new ArgumentException("Unresolved entity");
        }

        public static AdministratorAccount ResolveAdministrator(IAdministratorAccountRepository repository)
        {
            AdministratorAccount entity = repository.GetAdministrator();
            if (entity != null)
                return entity;

            throw new ArgumentException("Unresolved entity");
        }

        public static DispetcherAccount ResolveDispetcher(IDispetcherAccountRepository repository)
        {
            DispetcherAccount entity = repository.GetDispetcher();
            if (entity != null)
                return entity;

            throw new ArgumentException("Unresolved entity");
        }
    }
}
