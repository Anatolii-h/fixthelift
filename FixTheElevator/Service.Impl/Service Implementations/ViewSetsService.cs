﻿using FixTheElevator.Model;
using FixTheElevator.Repository;
using System.Collections.Generic;

namespace FixTheElevator.Service.Impl
{
    class ViewSetsService : IViewSetsService
    {
        private IWorkerAccountRepository workerAccountRepository;
        private IAdministratorAccountRepository administratorAccountRepository;
        private IDispetcherAccountRepository dispetcherAccountRepository;
        private IOrderRepository orderRepository;

        public ViewSetsService(IAdministratorAccountRepository administratorAccountRepository
            , IDispetcherAccountRepository dispetcherAccountRepository
            , IWorkerAccountRepository workerAccountRepository
            , IOrderRepository orderRepository)
        {
            this.administratorAccountRepository = administratorAccountRepository;
            this.dispetcherAccountRepository = dispetcherAccountRepository;
            this.workerAccountRepository = workerAccountRepository;
            this.orderRepository = orderRepository;
        }

        public List<AdministratorAccount> ViewAdministrators()
        {
            return administratorAccountRepository.ToList();
        }

        public List<DispetcherAccount> ViewDispetchers()
        {
            return dispetcherAccountRepository.ToList();
        }

        List<Order> IViewSetsService.ViewOrders()
        {
            return orderRepository.ToList();
        }

        List<WorkerAccount> IViewSetsService.ViewWorkers()
        {
            return workerAccountRepository.ToList();
        }
    }
}
