﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FixTheElevator.Model;
using FixTheElevator.Dto;

namespace FixTheElevator.Service.Impl
{
    static class DtoBuilder
    {
        public static AccountDto ToDto(Account account)
        {
            return new AccountDto(account.Id, account.FirstName, account.LastName,account.Contact);
        }

        public static WorkerAccountDto ToDto (WorkerAccount worker)
        {
            return new WorkerAccountDto(worker.Id, worker.FirstName, worker.LastName, worker.Contact);
        }

        public static AdministratorAccountDto ToDto(AdministratorAccount admin)
        {
            return new AdministratorAccountDto(admin.Id, admin.FirstName, admin.LastName, admin.Contact);
        }

        public static DispetcherAccountDto ToDto(DispetcherAccount disp)
        {
            return new DispetcherAccountDto(disp.Id, disp.FirstName, disp.LastName, disp.Contact);
        }

        public static AdressDto ToDto(Address adress)
        {
            return new AdressDto(
                adress.Id,
                adress.District,
                adress.Street,
                adress.HouseNumber,
                adress.EnteranceNumber
            );
        }

        public static ContactsDto ToDto(Contacts con)
        {
            return new ContactsDto(con.Id, con.Phone, con.EMail, con.address);
        }

        public static OrderDto ToDto(Order order)
        {
            return new OrderDto(
                order.Id, 
                order.dateTime, 
                order.address,
                order.Description, 
                order.Status
            );
        }
    }
}
