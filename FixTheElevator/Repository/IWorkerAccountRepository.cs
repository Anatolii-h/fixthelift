﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FixTheElevator.Model;

namespace FixTheElevator.Repository
{
    interface IWorkerAccountRepository : IRepository<WorkerAccount>
    {
        WorkerAccount FindWorkerById(Guid id);
        WorkerAccount FindWorkerByName(string fName, string lName);
        WorkerAccount FindFreeWorkerByDistrict(string district);
    }
}
