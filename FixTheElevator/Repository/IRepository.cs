﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixTheElevator.Repository
{
    public interface IRepository<T>
    {
        void StartTransaction();

        void Commit();

        void Rollback();

        int Count();

        T Load(int id);

        List<T> ToList();

        void Add(T t);

        void Delete(T t);
    }
}
