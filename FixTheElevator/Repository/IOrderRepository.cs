﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FixTheElevator.Model;

namespace FixTheElevator.Repository
{
    interface IOrderRepository : IRepository<Order>
    {
        Order GetOrderById(Guid id);
    }
}
