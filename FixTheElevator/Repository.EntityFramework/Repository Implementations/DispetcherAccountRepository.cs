﻿using FixTheElevator.Model;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace FixTheElevator.Repository.EntityFramework
{
    class DispetcherAccountRepository : BasicRepository<DispetcherAccount>, IDispetcherAccountRepository
    {
        public DispetcherAccountRepository(LiftDataContext dbContext)
            : base(dbContext, dbContext.Dispetchers)
        { }
        public DispetcherAccount GetDispetcher()
        {
            DispetcherAccount da = GetDBSet().FirstOrDefault();

            GetDBContext().Entry(da).Reference("Contact").Load();
            GetDBContext().Entry(da.Contact).Reference("address").Load();

            return da;
        }

        public override List<DispetcherAccount> ToList()
        {
            List<DispetcherAccount> list = GetDBSet().ToList<DispetcherAccount>();

            DbContext context = GetDBContext();

            for (int i = 0; i < list.Count(); i++)
            {
                context.Entry(list[i]).Reference("Contact").Load();
                context.Entry(list[i].Contact).Reference("address").Load();
            }

            return list;
        }
    }
}
