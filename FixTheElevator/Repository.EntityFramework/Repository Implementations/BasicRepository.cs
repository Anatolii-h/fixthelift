﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FixTheElevator.Model;

namespace FixTheElevator.Repository.EntityFramework
{
    abstract class BasicRepository<T> where T : class
    {
        private LiftDataContext dbContext;
        private DbSet<T> dbSet;

        protected BasicRepository(LiftDataContext dbContext, DbSet<T> dbSet)
        {
            this.dbContext = dbContext;
            this.dbSet = dbSet;
        }

        public LiftDataContext GetDBContext()
        {
            return this.dbContext;
        }

        public virtual List<T> ToList()
        {
            return this.dbSet.ToList<T>();
        }

        public DbSet<T> GetDBSet()
        {
            return this.dbSet;
        }

        public void Add(T obj)
        {
            dbSet.Add(obj);
        }

        public void Delete(T obj)
        {
            dbSet.Remove(obj);
        }

        public IQueryable<T> LoadAll()
        {
            return dbSet;
        }

        public T Load(int id)
        {
            return dbSet.Find(id);
        }

        public int Count()
        {
            return dbSet.Count();
        }

        public void StartTransaction()
        {
            this.dbContext.Database.BeginTransaction();
        }

        public void Commit()
        {
            this.dbContext.ChangeTracker.DetectChanges();
            this.dbContext.SaveChanges();
            this.dbContext.Database.CurrentTransaction.Commit();
        }

        public void Rollback()
        {
            this.dbContext.Database.CurrentTransaction.Rollback();
        }
    }
}