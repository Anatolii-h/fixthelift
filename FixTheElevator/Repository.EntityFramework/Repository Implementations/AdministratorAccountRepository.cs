﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using FixTheElevator.Model;

namespace FixTheElevator.Repository.EntityFramework
{
    class AdministratorAccountRepository: BasicRepository<AdministratorAccount>, IAdministratorAccountRepository
    {
        public AdministratorAccountRepository(LiftDataContext dbContext)
            : base(dbContext, dbContext.Administrators)
        { }
        public AdministratorAccount GetAdministrator()
        {
            AdministratorAccount aa = GetDBSet().FirstOrDefault();

            GetDBContext().Entry(aa).Reference("Contact").Load();
            GetDBContext().Entry(aa.Contact).Reference("address").Load();

            return aa;
        }

        public override List<AdministratorAccount> ToList()
        {
            List<AdministratorAccount> list = GetDBSet().ToList<AdministratorAccount>();

            DbContext context = GetDBContext();

            for (int i = 0; i < list.Count(); i++)
            {
                context.Entry(list[i]).Reference("Contact").Load();
                context.Entry(list[i].Contact).Reference("address").Load();
            }

            return list;
        }
    }
}
