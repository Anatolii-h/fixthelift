﻿using FixTheElevator.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace FixTheElevator.Repository.EntityFramework
{
    class WorkerAccountRepository : BasicRepository<WorkerAccount>, IWorkerAccountRepository
    {

        public WorkerAccountRepository(LiftDataContext dbContext)
            : base(dbContext, dbContext.Workers)
        {}

        public WorkerAccount FindWorkerById(Guid id)
        {
            WorkerAccount wa = GetDBSet().Where(a => a.Id == id).SingleOrDefault();

            GetDBContext().Entry(wa).Reference("Contact").Load();
            GetDBContext().Entry(wa.Contact).Reference("address").Load();

            if (wa.Status == WorkerStatus.Busy)
            {
                GetDBContext().Entry(wa).Reference("CurrentOrder").Load();
                GetDBContext().Entry(wa.CurrentOrder).Reference("address").Load();
            }

            GetDBContext().Entry(wa).Collection("CompletedOrders").Load();

            return wa;
        }

        public WorkerAccount FindWorkerByName(string fName, string lName)
        {
            WorkerAccount wa = GetDBSet().Where(a => a.FirstName == fName && a.LastName == lName).SingleOrDefault();

            GetDBContext().Entry(wa).Reference("Contact").Load();
            GetDBContext().Entry(wa.Contact).Reference("address").Load();

            if (wa.Status == WorkerStatus.Busy)
            {
                GetDBContext().Entry(wa).Reference("CurrentOrder").Load();
                GetDBContext().Entry(wa.CurrentOrder).Reference("address").Load();
            }

            GetDBContext().Entry(wa).Collection("CompletedOrders").Load();

            return wa;
        }

        public WorkerAccount FindFreeWorkerByDistrict(string district)
        {
            WorkerAccount wa = GetDBSet().Where(a => a.Status == WorkerStatus.Free && a.Contact.address.District == district).SingleOrDefault();

            GetDBContext().Entry(wa).Reference("Contact").Load();
            GetDBContext().Entry(wa.Contact).Reference("address").Load();

            if (wa.Status == WorkerStatus.Busy)
            {
                GetDBContext().Entry(wa).Reference("CurrentOrder").Load();
                GetDBContext().Entry(wa.CurrentOrder).Reference("address").Load();
            }

            GetDBContext().Entry(wa).Collection("CompletedOrders").Load();

            return wa;
        }

        public override List<WorkerAccount> ToList()
        {
            List<WorkerAccount> list = GetDBSet().ToList<WorkerAccount>();

            DbContext context = GetDBContext();

            for (int i = 0; i < list.Count(); i++)
            {
                context.Entry(list[i]).Reference("Contact").Load();
                context.Entry(list[i].Contact).Reference("address").Load();

                if (list[i].Status == WorkerStatus.Busy)
                {
                    GetDBContext().Entry(list[i]).Reference("CurrentOrder").Load();
                    GetDBContext().Entry(list[i].CurrentOrder).Reference("address").Load();
                }

                GetDBContext().Entry(list[i]).Collection("CompletedOrders").Load();
            }

            return list;
        }
    }
}
