﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FixTheElevator.Model;

namespace FixTheElevator.Repository.EntityFramework
{
    class AddressRepository : BasicRepository<Address>, IAddressRepository

    {
        public AddressRepository(LiftDataContext dbContext)
            : base(dbContext, dbContext.Addresses)
        { }

        public Address GetAddressById(Guid id)
        {
            return GetDBSet().Where(a => a.Id == id).SingleOrDefault();
        }
    }
}
