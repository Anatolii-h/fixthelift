﻿using FixTheElevator.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace FixTheElevator.Repository.EntityFramework
{
    class ContactsRepository : BasicRepository<Contacts>, IContactsRepository
    {
        public ContactsRepository(LiftDataContext dbContext)
            : base(dbContext, dbContext.Contacts)
        {}

        public Contacts GetContactsById(Guid id)
        {
            return GetDBSet().Include("address").Where(a => a.Id == id).SingleOrDefault();
        }

        public override List<Contacts> ToList()
        {
            List<Contacts> list = GetDBSet().ToList<Contacts>();

            DbContext context = GetDBContext();

            for (int i = 0; i < list.Count(); i++)
            {
                context.Entry(list[i]).Reference("address").Load();
            }

            return list;
        }
    }
}
