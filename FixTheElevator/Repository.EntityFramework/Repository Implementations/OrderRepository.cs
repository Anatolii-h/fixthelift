﻿using FixTheElevator.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace FixTheElevator.Repository.EntityFramework
{
    class OrderRepository : BasicRepository<Order>, IOrderRepository
    {

        public OrderRepository(LiftDataContext dbContext)
            : base(dbContext, dbContext.Orders)
        { }

        public Order GetOrderById(Guid id)
        {
            return GetDBSet().Include("address").Where(a => a.Id == id).SingleOrDefault();
        }

        public override List<Order> ToList()
        {
            List<Order> list = GetDBSet().ToList<Order>();

            DbContext context = GetDBContext();

            for (int i = 0; i < list.Count(); i++)
            {
                context.Entry(list[i]).Reference("address").Load();
            }

            return list;
        }
    }
}
