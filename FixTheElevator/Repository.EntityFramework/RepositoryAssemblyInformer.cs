﻿using System.Reflection;

namespace FixTheElevator.Repository.EntityFramework
{
    static class RepositoryAssemblyInformer
    {
        public static AssemblyName assemblyName { get; set; }

        static RepositoryAssemblyInformer()
        {
            assemblyName = Assembly.GetExecutingAssembly().GetName();
        }
    }
}
