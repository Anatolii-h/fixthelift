﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixTheElevator.Exceptions
{
    class DbSetIsEmptyException : DbSetException
    {
        public DbSetIsEmptyException(Type typeOfObjects, string message) : base(typeOfObjects, message)
        {}
    }
}
