﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixTheElevator.Exceptions
{
    class EntityNotFoundException : DbSetException
    {
        public object Identifier { get; private set; }

        public EntityNotFoundException(object identifier, Type typeOfObjects, string message) : base(typeOfObjects, message)
        {
            Identifier = identifier;
        }
    }
}
