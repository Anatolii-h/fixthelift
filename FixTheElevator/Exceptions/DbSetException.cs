﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixTheElevator.Exceptions
{
    class DbSetException : DomainException
    {
        public Type ObjectsOfSet { get; private set; }

        public DbSetException(Type typeOfObjects, string message) : base(message)
        {
            ObjectsOfSet = typeOfObjects;
        }
    }
}
