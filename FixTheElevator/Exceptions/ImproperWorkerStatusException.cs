﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FixTheElevator.Model;

namespace FixTheElevator.Exceptions
{
    class ImproperWorkerStatusException : DomainException
    {
        public WorkerStatus CurrentStatus { get; private set; }
 
        public ImproperWorkerStatusException(WorkerStatus currentStatus, string message) : base(message)
        {
            CurrentStatus = currentStatus;
        }
    }
}
