﻿using FixTheElevator.Dependencies;
using FixTheElevator.Model;
using FixTheElevator.Models;
using FixTheElevator.Repository.EntityFramework;
using FixTheElevator.Service;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FixTheElevator.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View(GetResentOrders());
        }

        [HttpPost]
        public ActionResult RegisterOrder(SimpleOrder so)
        {
            using (var dbContext = new LiftDataContext())
            using (var unityContainer = new UnityContainer())
            {
                dbContext.Database.Initialize(true);

                ContainerBoostraper.RegisterTypes(unityContainer, dbContext);

                var service = unityContainer.Resolve<IAddEntityToSetService>();

                Order order = new Order(new Address(so.District, so.Street, so.HouseNumber, so.EnteranceNumber), so.Description, DateTime.Now);

                service.AddOrder(order);
            }

                return View("~/Views/Home/Index.cshtml", GetResentOrders());
        }

        public IEnumerable<Order> GetResentOrders()
        {
            IEnumerable<Order> orders;

            using (var dbContext = new LiftDataContext())
            using (var unityContainer = new UnityContainer())
            {
                dbContext.Database.Initialize(true);

                ContainerBoostraper.RegisterTypes(unityContainer, dbContext);

                var viewer_service = unityContainer.Resolve<IViewSetsService>();

                int count = viewer_service.ViewOrders().Count;

                if (count >= 3)
                    orders = new Order[] { viewer_service.ViewOrders()[count - 1], viewer_service.ViewOrders()[count - 2], viewer_service.ViewOrders()[count - 3] };
                else if (count == 2)
                    orders = new Order[] { viewer_service.ViewOrders()[count - 1], viewer_service.ViewOrders()[count - 2] };
                else if (count == 1)
                    orders = new Order[] { viewer_service.ViewOrders()[count - 1] };
                else
                    orders = new Order[0];
            }

            return orders;
        }
    }
}