﻿using System;
using System.Linq;
using System.Reflection;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.InterceptionExtension;
using Microsoft.Practices.EnterpriseLibrary.Validation.PolicyInjection;
using FixTheElevator.Repository.EntityFramework;
using FixTheElevator.Service;
using FixTheElevator.Service.Impl;
using FixTheElevator.Repository;

namespace FixTheElevator.Dependencies
{
    static class ContainerBoostraper
    {
        public static void RegisterTypes(IUnityContainer container, LiftDataContext dbContext)
        {
            container.AddNewExtension<Interception>();

            container.RegisterInstance<LiftDataContext>(dbContext);

            RegisterLogFacilities(container);
            RegisterServices(container);
            RegisterRepositories(container);

            PrintContainerDebuggingInfo( container );
        }

        private static void RegisterLogFacilities(IUnityContainer container)
        {
            container.RegisterInstance<LiftEventSource>(new LiftEventSource());
            container.RegisterType(
                typeof(LogListener),
                new ContainerControlledLifetimeManager()
            );

            var logListener = container.Resolve<LogListener>();
            logListener.OnStartup();
        }

        private static void RegisterServices(IUnityContainer container)
        {
            container.Configure<Interception>()
                .AddPolicy("ValidationPolicy")
                    .AddMatchingRule<NamespaceMatchingRule>(
                        new InjectionConstructor("FixTheElevator.Service.Impl", true)
                    )
                    .AddCallHandler(
                        new ValidationCallHandler("", SpecificationSource.Both)
                    )
                    ;

            container.RegisterType<IAddEntityToSetService, AddEntityToSetService>();
            container.RegisterType<IChangeSalaryService, ChangeSalaryService>();
            container.RegisterType<IFindWorkerService, FindWorkerService>();
            container.RegisterType<IManageWorkerService, ManageWorkerService>();
            container.RegisterType<IViewSetsService, ViewSetsService>();
        }

        private static void RegisterRepositories(IUnityContainer container)
        {
            container.RegisterType<IAddressRepository, AddressRepository>();
            container.RegisterType<IAdministratorAccountRepository, AdministratorAccountRepository>();
            container.RegisterType<IContactsRepository, ContactsRepository>();
            container.RegisterType<IDispetcherAccountRepository, DispetcherAccountRepository>();
            container.RegisterType<IOrderRepository, OrderRepository>();
            container.RegisterType<IWorkerAccountRepository, WorkerAccountRepository>();
        }

        private static void PrintContainerDebuggingInfo(IUnityContainer container)
        {
            Console.WriteLine("Container has {0} Registrations:", container.Registrations.Count());

            foreach (ContainerRegistration item in container.Registrations)
                Console.WriteLine(item.GetMappingAsString());
        }

        private static string GetMappingAsString(this ContainerRegistration registration)
        {
            string regName, regType, mapTo, lifetime;

            var r = registration.RegisteredType;
            regType = r.Name + GetGenericArgumentsList(r);

            var m = registration.MappedToType;
            mapTo = m.Name + GetGenericArgumentsList(m);

            regName = registration.Name ?? "[default]";

            lifetime = registration.LifetimeManagerType.Name;
            if (mapTo != regType)
                mapTo = " -> " + mapTo;
            else
                mapTo = string.Empty;

            lifetime = lifetime.Substring(0, lifetime.Length - "LifetimeManager".Length);
            return String.Format("+ {0}{1}  '{2}'  {3}", regType, mapTo, regName, lifetime);
        }

        private static string GetGenericArgumentsList(Type type)
        {
            if (type.GetGenericArguments().Length == 0)
                return string.Empty;

            string arglist = string.Empty;
            bool first = true;
            foreach (Type t in type.GetGenericArguments())
            {
                arglist += first ? t.Name : ", " + t.Name;
                first = false;
                if (t.GetGenericArguments().Length > 0)
                    arglist += GetGenericArgumentsList(t);
            }
            return "<" + arglist + ">";
        }
    }
}