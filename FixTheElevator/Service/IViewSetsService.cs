﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FixTheElevator.Model;

namespace FixTheElevator.Service
{
    interface IViewSetsService
    {
        List<Order> ViewOrders();
        List<WorkerAccount> ViewWorkers();
        List<DispetcherAccount> ViewDispetchers();
        List<AdministratorAccount> ViewAdministrators();
    }
}
