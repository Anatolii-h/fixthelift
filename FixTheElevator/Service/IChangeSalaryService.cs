﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FixTheElevator.Model;
using FixTheElevator.Utils.Validators;

namespace FixTheElevator.Service
{
    interface IChangeSalaryService
    {
        void ChangeAdministratorsSalary([SalaryValidator] int newSalary);
        void ChangeDispetchersSalary([SalaryValidator] int newSalary);
        void ChangeWorkersSalary(Guid Id, [SalaryValidator] int newSalary);
    }
}
