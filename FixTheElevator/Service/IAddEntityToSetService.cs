﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FixTheElevator.Model;

namespace FixTheElevator.Service
{
    interface IAddEntityToSetService
    {
        void AddOrder(Order order);
        void AddWorker(WorkerAccount worker);
        void AddDispetcher(DispetcherAccount dispetcher);
        void AddAdministrator(AdministratorAccount administrator);
    }
}
