﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FixTheElevator.Dto;
using FixTheElevator.Model;

namespace FixTheElevator.Service
{
    interface IFindWorkerService
    {
        WorkerAccountDto FindFreeWorkerByDistrict(string district);
        WorkerAccountDto FindWorkerByName(string firstName, string lastName);
        WorkerAccountDto FindWorkerById(Guid id);
    }
}
