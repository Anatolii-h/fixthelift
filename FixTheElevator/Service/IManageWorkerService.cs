﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FixTheElevator.Model;

namespace FixTheElevator.Service
{
    interface IManageWorkerService
    {
        void AssignOrderToWorker(Guid workerId, Guid orderId);
        void CompleteOrder(Guid workerId);
    }
}
