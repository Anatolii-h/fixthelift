﻿using System;

using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;

namespace FixTheElevator.Utils.Validators
{

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter)]
    public class EmailValidator : ValidatorAttribute
    {
        protected override Validator DoCreateValidator(Type targetType)
        {
            const string EmailRegex = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
            return new RegexValidator(EmailRegex);
        }
    }
}
