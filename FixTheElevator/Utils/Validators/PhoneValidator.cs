﻿using System;

using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;

namespace FixTheElevator.Utils.Validators
{

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter)]
    public class PhoneValidator : ValidatorAttribute
    {
        protected override Validator DoCreateValidator(Type targetType)
        {
            const string PhoneRegex = @"([0-9\s\-]{7,})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?";
            return new RegexValidator(PhoneRegex);
        }
    }
}