﻿using System;

using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;

namespace FixTheElevator.Utils.Validators
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter)]
    class SalaryValidator : ValidatorAttribute
    {
        protected override Validator DoCreateValidator(Type targetType)
        {
            return new RangeValidator(
                            0.0M,
                            RangeBoundaryType.Inclusive,
                            decimal.MaxValue,
                            RangeBoundaryType.Ignore
                      );
        }
    }
}
